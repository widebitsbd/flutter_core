import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class ThemeState extends Equatable {
  final ThemeData theme;
  final bool isDark;

  const ThemeState({@required this.theme, @required this.isDark})
      : assert(theme != null),
        assert(isDark != null);

  @override
  List<Object> get props => [theme, isDark];
}
