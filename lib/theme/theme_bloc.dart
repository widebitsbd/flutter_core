import 'dart:async';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core/core/env.dart';
import 'package:flutter_core/pref/app_pref.dart';
import 'package:flutter_core/pref/pref_key.dart';
import 'package:flutter_core/theme/theme_event.dart';
import 'package:flutter_core/theme/theme_state.dart';


class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  final AppPreference _server;

  ThemeBloc({@required AppPreference server})
      : assert(server != null),
        _server = server, super(_getThemeState(server.getBoolean(key:PrefKey.IS_DARK_THEME)));

  static ThemeState _getThemeState(bool isDark) {
    return ThemeState(
        theme: ThemeData(
            brightness: isDark ? Brightness.dark : Brightness.light,
            fontFamily: 'Cabin',
            primarySwatch: Env.value.primarySwatch,
            buttonTheme: ButtonThemeData(shape: RoundedRectangleBorder())),
        isDark: isDark);
  }

  @override
  Stream<ThemeState> mapEventToState(ThemeEvent event) async* {
    switch (event) {
      case ThemeEvent.toggle:
        yield _mapThemeToggleToState(event);
    }
  }

  ThemeState _mapThemeToggleToState(ThemeEvent event) {
    _server.setBoolean(PrefKey.IS_DARK_THEME,!state.isDark);
    return _getThemeState(!state.isDark);
  }



}
