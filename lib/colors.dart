import 'package:flutter/material.dart';

const colorPrimary = const Color(0xFF097B40);

const colorPrimaryDark = const Color(0xFF054423);

const colorWhite = Colors.white;
const colorLightGrey = const Color(0xFFF1F3F6);
const colorGrey = const Color(0x05333333);
const colorDarkGrey = const Color(0xFFCBCBCB);
const colorTextLightGrey = const Color(0xFF555555);
const colorTextDarkGrey = const Color(0xFF414141);
const colorYellow = const Color(0xFFF1A137);
const colorRed = const Color(0xFFE23557);
const colorAshGreen = const Color(0xFF407E7D);
const colorOffGreen = const Color(0xFF78C398);
const coronaDashboardTitleColor = const Color(0x7335915A);
