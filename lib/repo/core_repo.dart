import 'package:flutter_core/api/api_client.dart';
import 'package:flutter_core/pref/app_pref.dart';
import 'package:sembast/sembast.dart';

class CoreRepo{
  final ApiClient _api;
  final Database _db;
  final AppPreference _pref;
  StoreRef _store;

  CoreRepo(this._api, this._db, this._pref){
    _store = StoreRef.main();
  }

}