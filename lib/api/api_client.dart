import 'package:dio/dio.dart';
import 'package:flutter_core/api/base_url.dart';
import 'package:flutter_core/exception/HttpException.dart';
import 'package:flutter_core/logger/DioLogger.dart';

class ApiClient {
  Dio _dio;

  ApiClient() {
    BaseOptions dioOptions = BaseOptions()..baseUrl = BaseUrl.baseURL;
    _dio = Dio(dioOptions);
    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      DioLogger.onSend(options);
      return options;
    }, onResponse: (Response response) {
        DioLogger.onSuccess(response);
      return response;
    }, onError: (DioError error) {
        DioLogger.onError(error);
      return error;
    }));
  }

  Future<Response> getFutureResponse(String url) async {
    Response response = await _dio.get(url);
    throwIfNoSuccess(response);
    return response;
  }

  Stream<Response> getStreamResponse(String url) async* {
    Response response = await _dio.get(url);
    throwIfNoSuccess(response);
    yield response;
  }

  void throwIfNoSuccess(Response response) {
    if (response.statusCode < 200 || response.statusCode > 299) {
      throw HttpException(response);
    }
  }
}
