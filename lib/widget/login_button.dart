import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget {
  final String _text;
  final VoidCallback _onPressed;

  LoginButton({Key key, String text, VoidCallback onPressed})
      : _text = text,
        _onPressed = onPressed,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      key: ValueKey(_text),
      color: Theme.of(context).primaryColorDark,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      onPressed: _onPressed,
      child: Text(
        _text,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}
