import 'package:flutter/material.dart';
import 'package:flutter_core/utils/lang_util.dart';
import 'package:flutter_core/user/repo/firebase_user_repository.dart';


class CreateAccountButton extends StatelessWidget {
  //ignore: unused_field
  final FirebaseUserRepository _userRepository;

  CreateAccountButton({Key key, @required FirebaseUserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text(
        'label.not_a_member_create_account'.lang,
      ),
      onPressed: () {
       // BlocProvider.of<AuthenticationBloc>(context)..add(CreateAccount());
      },
    );
  }
}
