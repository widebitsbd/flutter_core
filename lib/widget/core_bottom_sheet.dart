import 'package:flutter/material.dart';

class CoreBottomSheet extends StatefulWidget {
  @override
  _CoreBottomSheetState createState() => _CoreBottomSheetState();
}

class _CoreBottomSheetState extends State<CoreBottomSheet> {
  int index = -1;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        height: 270,
        child: GridView.extent(
          maxCrossAxisExtent: 330.0,
          childAspectRatio: 2.4,
          shrinkWrap: true,
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 8.0,
          children: List.generate(12, (index) {
            return Stack(
              children: <Widget>[
                InkWell(
                  onTap: () => _onTapItem(index),
                  child: Container(
                    width: 180,
                    height: 72,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      border: Border.all(
                        width: 2.0,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                    child:
                        Image.network('https://randomuser.me/api/portraits/women/0.jpg'),
                  ),
                ),
                Align(alignment: Alignment.bottomCenter, child: Text('Universe')),
                Align(alignment: Alignment.bottomRight, child: _getCheckedWidget(index)),
              ],
            );
          }),
        ),
      ),
    );
  }

  Widget _getCheckedWidget(int ind) {
    if (index == ind) return Icon(Icons.check);
    return SizedBox();
  }

  void _onTapItem(int ind) {
    setState(() {
      index = ind;
    });
  }
}
