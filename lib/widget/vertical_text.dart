import 'package:flutter/material.dart';

class VerticalText extends StatelessWidget {
  final String text;

  const VerticalText({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 72, left: 8),
      child: RotatedBox(
          quarterTurns: -1,
          child: Text(
            text,
            style: TextStyle(
              color: Theme.of(context).primaryColorDark,
              fontSize: 26,
              fontWeight: FontWeight.w900,
            ),
          )),
    );
  }
}
