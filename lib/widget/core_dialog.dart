import 'package:flutter/material.dart';


class CoreDialog {
  void showSnackBar(GlobalKey<ScaffoldState> key, String txt) {
    key.currentState.showSnackBar(SnackBar(content: Text(txt)));
  }

  void snackBar(BuildContext context, String message, {bool isError = false}) {
    Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          backgroundColor: isError ? Colors.red : null,
          content: Text(message),
        ),
      );
  }

  void snackBarProgress(BuildContext context, String message) {
    Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          duration: Duration(seconds: 20),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                message,
                overflow: TextOverflow.ellipsis,
              ),
              CircularProgressIndicator(),
            ],
          ),
        ),
      );
  }


  static void confirmDialog(
      BuildContext context, String title, String content, VoidCallback callback) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(title),
            content: new Text(content),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Ok"),
                onPressed: () async {
                  Navigator.of(context).pop();
                  callback();
                },
              ),
              new FlatButton(
                child: new Text("Cancel"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }



  void removeDialog(context, title, content, callback) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Remove"),
              color: Colors.red,
              onPressed: () {
                Navigator.of(context).pop();
                callback();
              },
            ),
          ],
        );
      },
    );
  }

}
