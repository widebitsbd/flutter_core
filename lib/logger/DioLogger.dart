import 'package:dio/dio.dart';
import 'package:flutter_core/logger/Log.dart';

class DioLogger {
  static void onSend(RequestOptions options) {
    Log.d('Request Path: [${options.method}] ${options.baseUrl}${options.path}'
        '\nRequest Data: ${options.data.toString()}');
  }

  static void onSuccess(Response response) {
    Log.d(
        'Response Path: [${response.request.method}] ${response.request.baseUrl}${response.request.path}'
        '\nRequest Data: ${response.request.data.toString()}'
        '\nResponse statusCode:  ${response.statusCode}'
        '\nResponse data: ${response.data.toString()}');
  }

  static void onError(DioError error) {
    String msg = '';
    if (null != error.response) {
      msg =
          'Error Path: [${error.response.request.method}] ${error.response.request.baseUrl}${error.response.request.path}'
          '\nRequest Data: ${error.response.request.data.toString()}'
          '\nError statusCode: ${error.response.statusCode}'
          '\nError data : ${null != error.response.data ? error.response.data.toString() : ''}';
    }
    msg += 'Error Message: ${error.message}';
    Log.e(msg);
  }
}
