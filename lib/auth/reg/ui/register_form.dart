import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_core/auth/bloc/bloc.dart';
import 'package:flutter_core/widget/core_dialog.dart';
import 'package:flutter_core/widget/login_button.dart';
import 'package:flutter_core/widget/vertical_text.dart';
import 'package:flutter_core/utils/lang_util.dart';
import '../bloc/bloc.dart';

class RegisterForm extends StatefulWidget {
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final focusLastName = FocusNode();
  final focusEmail = FocusNode();
  final focusPassword = FocusNode();

  RegisterBloc _registerBloc;
  bool _showPassword = false;

  bool get isPopulated =>
      _firstNameController.text.isNotEmpty &&
      _lastNameController.text.isNotEmpty &&
      _emailController.text.isNotEmpty &&
      _passwordController.text.isNotEmpty;

  bool isRegisterButtonEnabled(RegisterState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
      cubit: _registerBloc,
      listener: (context, state) {
        if (state.isSubmitting)
          CoreDialog().snackBarProgress(context, tr('message.registering'));
        else if (state.successMsg.isNotEmpty) {
          BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
        } else if (state.failureMsg.isNotEmpty)
          CoreDialog().snackBar(context, tr(state.failureMsg), isError: true);
      },
      child: BlocBuilder<RegisterBloc, RegisterState>(
        builder: (context, state) {
          return _buildBody(context, state);
        },
      ),
    );
  }

  Widget _buildBody(context, RegisterState state) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        VerticalText(text: tr('label.create_an_account')),
        Expanded(
          child: Form(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 48),
              child: ListView(
                children: <Widget>[
                  Image.asset('assets/images/app_icon_256.png', height: 64),
                  SizedBox(
                    height: 16,
                  ),
                  TextFormField(
                      controller: _firstNameController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          filled: true,
                          icon: Icon(Icons.person),
                          hintText: 'What should we call you?',
                          labelText: 'First name',
                          errorText: state.firstNameError.isNullOrEmpty
                              ? null
                              : tr(state.firstNameError)),
                      autocorrect: false,
                      textCapitalization: TextCapitalization.words,
                      textInputAction: TextInputAction.next,
                      onEditingComplete: _onFirstNameChanged,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context).requestFocus(focusLastName);
                      }),
                  TextFormField(
                      controller: _lastNameController,
                      focusNode: focusLastName,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          filled: true,
                          icon: Icon(Icons.person),
                          hintText: 'And your last name?',
                          labelText: 'Last name',
                          errorText: state.lastNameError.isNullOrEmpty
                              ? null
                              : tr(state.lastNameError)),
                      autocorrect: false,
                      onEditingComplete: _onLastNameChanged,
                      textCapitalization: TextCapitalization.words,
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context).requestFocus(focusEmail);
                      }),
                  TextFormField(
                      controller: _emailController,
                      focusNode: focusEmail,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          filled: true,
                          icon: Icon(Icons.email),
                          hintText: 'What is your email id?',
                          labelText: 'Email',
                          errorText: state.emailError.isNullOrEmpty
                              ? null
                              : tr(state.emailError)),
                      autocorrect: false,
                      onEditingComplete: _onEmailChanged,
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context).requestFocus(focusPassword);
                      }),
                  TextFormField(
                    controller: _passwordController,
                    focusNode: focusPassword,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        filled: true,
                        icon: Icon(Icons.lock),
                        hintText: 'Password? Use a strong one',
                        labelText: 'Password',
                        suffixIcon: IconButton(
                          icon: Icon(Icons.remove_red_eye),
                          color: _showPassword ? Colors.blue : Colors.grey,
                          onPressed: () {},
                        ),
                        errorText: state.passwordError.isNullOrEmpty
                            ? null
                            : tr(state.passwordError)),
                    obscureText: true,
                    autocorrect: false,
                    onEditingComplete: _onPasswordChanged,
                    onFieldSubmitted: (_) {
                      FocusScope.of(context).unfocus();
                    },
                  ),
                  TextFormField(
                    readOnly: true,
                    decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        filled: true,
                        icon: Icon(Icons.supervised_user_circle),
                        hintText: tr('hint.tap_button_to_select'),
                        labelText: tr('label.user_type'),
                        suffixIcon: IconButton(
                          icon: Icon(Icons.change_history),
                          onPressed: () {
                            // CoreDialog().radioButtonBottomSheet(
                            //     context,
                            //     tr('label.user_type'),
                            //     tr('hint.select_user_type'),
                            //     [tr('label.normal'), tr('label.business')],
                            //     state.userType.isNullOrEmpty
                            //         ? tr('label.normal')
                            //         : tr(state.userType), (item) {
                            //   Log.d(item);
                            // });
                          },
                        )),
                  ),
                  TextFormField(
                    readOnly: true,
                    decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        filled: true,
                        icon: Icon(Icons.supervised_user_circle),
                        hintText: tr('hint.tap_button_to_select'),
                        labelText: tr('label.gender'),
                        suffixIcon: IconButton(
                          icon: Icon(Icons.change_history),
                          onPressed: () {
                            // CoreDialog().radioButtonBottomSheet(
                            //     context,
                            //     tr('label.gender'),
                            //     tr('hint.select_gender'),
                            //     [tr('label.male'), tr('label.female'), tr('label.other')],
                            //     state.gender.isNullOrEmpty
                            //         ? tr('label.male')
                            //         : tr(state.gender), (item) {
                            //   Log.d(item);
                            // });
                          },
                        )),
                  ),
                  // DatePicker2(
                  //   labelText: tr('label.birth_date'),
                  //   selectedDate: DateTime.now(),
                  // ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(tr('message.eula'), style: TextStyle(fontSize: 10)),
                  SizedBox(height: 8),
                  LoginButton(
                    text: tr('label.register'),
                    onPressed: isRegisterButtonEnabled(state) ? _onFormSubmitted : null,
                  ),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                     // BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
                    },
                    child: Text(
                      tr('label.go_back_to_login'),
                      style: TextStyle(
                          color: Theme.of(context).brightness == Brightness.dark
                              ? Colors.black
                              : Colors.white),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onFirstNameChanged() {
    _registerBloc.add(
      FirstNameChanged(name: _firstNameController.text),
    );
  }

  void _onLastNameChanged() {
    _registerBloc.add(
      LastNameChanged(name: _lastNameController.text),
    );
  }

  void _onEmailChanged() {
    _registerBloc.add(
      EmailChanged(email: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _registerBloc.add(
      PasswordChanged(password: _passwordController.text),
    );
  }

  void _onFormSubmitted() {
    _registerBloc.add(
      Submitted(
          firstName: _firstNameController.text,
          lastName: _lastNameController.text,
          email: _emailController.text,
          password: _passwordController.text,
          gender: '',
          birthDate: 0,
          userType: ''),
    );
  }
}
