import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_core/auth/reg/ui/register_form.dart';
import 'package:flutter_core/auth/service/auth_service.dart';
import 'package:flutter_core/user/repo/firebase_user_repository.dart';
import '../bloc/bloc.dart';

class RegisterPage extends StatelessWidget {
  static const String PATH = '/register_page';
  final AuthService _authService;
  final FirebaseUserRepository _userRepository;

  RegisterPage(
      {Key key,
      @required FirebaseUserRepository userRepository,
      @required AuthService authService})
      : assert(userRepository != null),
        _userRepository = userRepository,
        _authService = authService,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.1), BlendMode.dstATop),
              image: CachedNetworkImageProvider(
                  'https://images.unsplash.com/photo-1472289065668-ce650ac443d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'),
              fit: BoxFit.cover),
        ),
        child: BlocProvider<RegisterBloc>(
          create: (context) => RegisterBloc(userRepository: _userRepository,authService: _authService),
          child: RegisterForm(),
        ),
      ),
    );
  }
}
