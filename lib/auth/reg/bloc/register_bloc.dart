import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_core/auth/service/auth_service.dart';
import 'package:flutter_core/logger/Log.dart';
import 'package:flutter_core/user/repo/firebase_user_repository.dart';
import 'package:flutter_core/utils/validation.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'bloc.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final FirebaseUserRepository _userRepository;
  final AuthService _authService;

  RegisterBloc({@required FirebaseUserRepository userRepository,@required AuthService authService})
      : assert(userRepository != null),
        _userRepository = userRepository,
        _authService = authService,
        super(RegisterState.init());

  @override
  Stream<Transition<RegisterEvent, RegisterState>> transformEvents(
    Stream<RegisterEvent> events,
    TransitionFunction<RegisterEvent, RegisterState> transitionFn,
  ) {
    final nonDebounceStream = events.where((event) {
      return (event is! EmailChanged && event is! PasswordChanged);
    });
    final debounceStream = events.where((event) {
      return (event is EmailChanged || event is PasswordChanged);
    }).debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      transitionFn,
   );
  }

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if (event is FirstNameChanged) {
      yield* _mapFirstNameChangeToState(event.name);
    }
    if (event is LastNameChanged) {
      yield* _mapLastNameChangeToState(event.name);
    } else if (event is EmailChanged) {
      yield* _mapEmailChangedToState(event.email);
    } else if (event is PasswordChanged) {
      yield* _mapPasswordChangedToState(event.password);
    } else if (event is Submitted) {
      yield* _mapFormSubmittedToState(
          event.email, event.firstName, event.lastName, event.password);
    }
  }

  Stream<RegisterState> _mapFirstNameChangeToState(String name) async* {
    yield state.copyWith(
      firstNameError: Validation.getNameValid(name),
    );
  }

  Stream<RegisterState> _mapLastNameChangeToState(String name) async* {
    yield state.copyWith(
      lastNameError: Validation.getNameValid(name),
    );
  }

  Stream<RegisterState> _mapEmailChangedToState(String email) async* {
    yield state.copyWith(
      emailError: Validation.getEmailValid(email),
    );
  }

  Stream<RegisterState> _mapPasswordChangedToState(String pass) async* {
    yield state.copyWith(
      passwordError: Validation.getPasswordValid(pass),
    );
  }

  Stream<RegisterState> _mapFormSubmittedToState(
    String email,
    String firstName,
    String lastName,
    String password,
  ) async* {
    if (Validation.getEmailValid(email).isEmpty &&
        Validation.getNameValid(firstName).isEmpty &&
        Validation.getNameValid(lastName).isEmpty &&
        Validation.getPasswordValid(password).isEmpty) {
      yield RegisterState.loading();
      try {
        // UserCredential result = await _userRepository.signUp(
        //   email: email,
        //   firstName: firstName,
        //   lastName: lastName,
        //   password: password,
        // );
        // Log.d(result.toString());
        // yield RegisterState.success(result.toString());
      } catch (e) {
        Log.e(e.toString());
        yield RegisterState.failure(e.toString());
      }
    } else {
      yield state.copyWith(
        firstNameError: Validation.getNameValid(firstName),
        lastNameError: Validation.getNameValid(lastName),
        emailError: Validation.getEmailValid(email),
        passwordError: Validation.getPasswordValid(password),
      );
    }
  }
}
