import 'package:meta/meta.dart';

@immutable
class RegisterState {
  final String firstNameError;
  final String lastNameError;
  final String emailError;
  final String passwordError;
  final bool isSubmitting;
  final String userType;
  final String gender;
  final int birthDate;
  final String successMsg;
  final String failureMsg;

  bool get isFormValid =>
      firstNameError.isEmpty &&
      lastNameError.isEmpty &&
      emailError.isEmpty &&
      passwordError.isEmpty;

  RegisterState({
    @required this.firstNameError,
    @required this.lastNameError,
    @required this.emailError,
    @required this.passwordError,
    @required this.isSubmitting,
    @required this.userType,
    @required this.gender,
    @required this.birthDate,
    @required this.successMsg,
    @required this.failureMsg,
  });

  factory RegisterState.init() {
    return RegisterState(
      firstNameError: '',
      lastNameError: '',
      emailError: '',
      passwordError: '',
      userType: '',
      gender: '',
      birthDate: -1,
      isSubmitting: false,
      successMsg: '',
      failureMsg: '',
    );
  }

  factory RegisterState.loading() {
    return RegisterState(
      firstNameError: '',
      lastNameError: '',
      emailError: '',
      passwordError: '',
      userType: '',
      gender: '',
      birthDate: -1,
      isSubmitting: true,
      successMsg: '',
      failureMsg: '',
    );
  }

  factory RegisterState.failure(String msg) {
    return RegisterState(
      firstNameError: '',
      lastNameError: '',
      emailError: '',
      passwordError: '',
      userType: '',
      gender: '',
      birthDate: -1,
      isSubmitting: false,
      successMsg: '',
      failureMsg: msg,
    );
  }

  factory RegisterState.success(String msg) {
    return RegisterState(
      firstNameError: '',
      lastNameError: '',
      emailError: '',
      passwordError: '',
      userType: '',
      gender: '',
      birthDate: -1,
      isSubmitting: false,
      successMsg: msg,
      failureMsg: '',
    );
  }

  RegisterState copyWith(
      {String firstNameError,
      String lastNameError,
      String emailError,
      String passwordError,
      String userType,
      String gender,
      int birthDate,
      bool isSubmitting,
      String successMsg,
      String failureMsg}) {
    return RegisterState(
      firstNameError: firstNameError ?? this.firstNameError,
      lastNameError: lastNameError ?? this.lastNameError,
      emailError: emailError ?? this.emailError,
      passwordError: passwordError ?? this.passwordError,
      userType: userType ?? this.userType,
      gender: gender ?? this.gender,
      birthDate: birthDate ?? this.birthDate,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      successMsg: successMsg ?? this.successMsg,
      failureMsg: failureMsg ?? this.failureMsg,
    );
  }

  @override
  String toString() {
    return 'RegisterState{'
        'firstNameError: $firstNameError, '
        'lastNameError: $lastNameError, '
        'emailError: $emailError, '
        'passwordError: $passwordError, '
        'isSubmitting: $successMsg, '
        'successMsg: $failureMsg, '
        'failureMsg: $failureMsg'
        '}';
  }
}
