import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_core/auth/bloc/auth_bloc.dart';
import 'package:flutter_core/auth/bloc/bloc.dart';

import 'guide_page.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  AuthenticationBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthenticationBloc>(context);
  }

  void _onIntroEnd(context) {
    _authBloc.add(OnBoardingDone());
  }

  @override
  Widget build(BuildContext context) {
    return GuidePage(() => _onIntroEnd(context));
  }
}
