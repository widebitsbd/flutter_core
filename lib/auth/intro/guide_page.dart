import 'package:flutter/material.dart';
import 'package:flutter_core/core/app_provider.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:flutter_core/utils/lang_util.dart';

class GuidePage extends StatelessWidget {
  static const String PATH = "/guide_page";
  final introKey = GlobalKey<IntroductionScreenState>();
  final VoidCallback onIntroEnd;

  GuidePage(this.onIntroEnd);

  Widget _buildImage(String assetName) {
    return Align(
      child: Image.asset('assets/images/$assetName.jpg', width: 350.0),
      alignment: Alignment.bottomCenter,
    );
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0);
    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
      key: introKey,
      pages: [
        PageViewModel(
          title: 'title.guide_1'.lang,
          body: "subtitle.guide_1".lang,
          image: _buildImage('intro1'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: 'title.guide_2'.lang,
          body: "subtitle.guide_2".lang,
          image: _buildImage('intro2'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: 'title.guide_3'.lang,
          body: "subtitle.guide_3".lang,
          image: _buildImage('intro3'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: 'title.guide_4'.lang,
          body: "subtitle.guide_4".lang,
          image: _buildImage('intro2'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: 'title.guide_5'.lang,
          body: "subtitle.guide_5".lang,
          image: _buildImage('intro1'),
          decoration: pageDecoration,
        ),
      ],
      onDone: onIntroEnd ?? () => {AppProvider.getRouter(context).pop(context)},
      onSkip: onIntroEnd ?? () => {AppProvider.getRouter(context).pop(context)},
      // You can override onSkip callback
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      skip: Text('label.skip'.lang),
      next: const Icon(Icons.arrow_forward),
      done: Text('label.done'.lang,
          style: TextStyle(fontWeight: FontWeight.w600)),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFFBDBDBD),
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
    );
  }
}
