import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_core/pref/app_pref.dart';
import 'package:flutter_core/pref/pref_key.dart';
import 'package:flutter_core/user/dto/logged_user.dart';
import 'package:meta/meta.dart';
import 'bloc.dart';

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState> {

  final AppPreference _pref;

  AuthenticationBloc(
      {@required AppPreference pref})
      : assert(pref != null),
        _pref = pref, super(Uninitialized());

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      yield* _mapAppStartedToState();
    } else if (event is LoggedIn) {
      yield* _mapLoggedInToState();
    } else if (event is LoggedOut) {
      yield* _mapLoggedOutToState();
    } else if (event is CreateAccount) {
      yield Unregistered();
    } else if (event is OnBoardingDone) {
      yield* _mapOnBoardingDoneToState();
    }
  }

  Stream<AuthenticationState> _mapAppStartedToState() async* {
    try {
      final isSignedIn =  _pref.getBoolean(key: PrefKey.IS_LOGGED_IN);
      if (isSignedIn) {
        final userId =  _pref.getInt(key:PrefKey.LOGGED_USER_ID);
        setLoggedInUser(userId);
        yield Authenticated(LoggedUser(id: userId));
      } else if (_pref.getBoolean(key: PrefKey.IS_ON_BOARDING)) {
        yield OnBoard();
      } else {
        yield Unauthenticated();
      }
    } catch (_) {
      yield Unauthenticated();
    }
  }

  void setLoggedInUser(int userId) {
    _pref.setInt(PrefKey.LOGGED_USER_ID, userId);
  }

  Stream<AuthenticationState> _mapLoggedInToState() async* {
    final userId = _pref.getInt(key:PrefKey.LOGGED_USER_ID);
    setLoggedInUser(userId);
    yield Authenticated(LoggedUser(id: userId));
  }

  Stream<AuthenticationState> _mapLoggedOutToState() async* {
    yield Unauthenticated();
  }

  Stream<AuthenticationState> _mapOnBoardingDoneToState() async* {
    _pref.getBoolean(key: PrefKey.IS_ON_BOARDING);
    yield Unauthenticated();
  }
}
