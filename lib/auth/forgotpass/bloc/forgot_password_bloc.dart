import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_core/auth/service/auth_service.dart';
import 'package:meta/meta.dart';

import 'bloc.dart';

class ForgotPasswordBloc extends Bloc<ForgotPasswordEvent, ForgotPasswordState> {
  final AuthService _server;

  ForgotPasswordBloc({
    @required AuthService server,})  : assert(server != null),
        _server = server, super(ForgotPasswordInitial());

  @override
  Stream<ForgotPasswordState> mapEventToState(
    ForgotPasswordEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
