import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_core/auth/forgotpass/bloc/bloc.dart';
import 'package:flutter_core/auth/service/auth_service.dart';
import 'package:flutter_core/widget/login_button.dart';
import 'package:flutter_core/widget/vertical_text.dart';
import 'package:flutter_core/utils/lang_util.dart';

class ForgotPasswordForm extends StatefulWidget {
  final AuthService _server;

  ForgotPasswordForm({Key key, @required AuthService server})
      : assert(server != null),
        _server = server,
        super(key: key);

  @override
  _ForgotPasswordFormState createState() => _ForgotPasswordFormState();
}

class _ForgotPasswordFormState extends State<ForgotPasswordForm> {
  final TextEditingController _emailController =
      TextEditingController(text: kReleaseMode ? '' : 'admin@email.com');

  @override
  void initState() {
    super.initState();
    //widget._server.debugAction();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordBloc, ForgotPasswordState>(
      listener: (context, state) {},
      child: BlocBuilder<ForgotPasswordBloc, ForgotPasswordState>(
        builder: (context, state) {
          return _buildBody(context, state);
        },
      ),
    );
  }

  Widget _buildBody(context, state) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        VerticalText(text: tr('title.title_forgot_password')),
        Expanded(
          child: Form(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 48),
              child: ListView(
                children: <Widget>[
                  Image.asset('images/app_icon_256.png', height: 64),
                  SizedBox(
                    height: 16,
                  ),
                  TextFormField(
                    controller: _emailController,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        filled: true,
                        icon: Icon(Icons.email),
                        hintText:  tr('hint.hint_what_is_your_email_id'),
                        labelText: tr('label.label_email')),
                    autocorrect: false,
                    autovalidate: true,
                    validator: (_) {
                      return null;
                      //return !state.isEmailValid ? 'Invalid Email' : null;
                    },
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  LoginButton(
                      text:  'label.label_send'.lang,
                      onPressed: () {}),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      tr('label.label_go_back_to_login'),
                      style: TextStyle(
                          color: Theme.of(context).brightness == Brightness.dark
                              ? Colors.black
                              : Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }
}
