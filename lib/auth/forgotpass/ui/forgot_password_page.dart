import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_core/auth/forgotpass/bloc/bloc.dart';
import 'package:flutter_core/auth/service/auth_service.dart';
import 'forgot_password_form.dart';

class ForgotPasswordPage extends StatelessWidget {
  static const String PATH = '/forgot_password';
  final AuthService _authService;

  ForgotPasswordPage({Key key, @required AuthService authService})
      : assert(authService != null),
        _authService = authService,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: CachedNetworkImageProvider(
                  'https://images.unsplash.com/photo-1579532648866-611dbe281eeb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'),
              fit: BoxFit.cover),
        ),
        child: BlocProvider<ForgotPasswordBloc>(
          create: (context) => ForgotPasswordBloc(server: _authService),
          child: ForgotPasswordForm(server: _authService),
        ),
      ),
    );
  }
}
