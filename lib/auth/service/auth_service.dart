import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_core/api/api_client.dart';
import 'package:flutter_core/auth/login/model/dto/login_response.dart';
import 'package:flutter_core/pref/app_pref.dart';
import 'package:flutter_core/repo/core_repo.dart';
import 'package:flutter_core/user/dto/logged_user.dart';

// ignore: implementation_imports
import 'package:sembast/src/api/v2/database.dart';

class AuthService extends CoreRepo {
  ApiClient api;
  Database db;
  AppPreference pref;

  AuthService(ApiClient api, Database db, AppPreference pref)
      : super(api, db, pref){
    this.api = api;
    this.db = db;
    this.pref = pref;
  }

  Future<LoggedUser> login (String userName, String password) async {
    try {
      var res = await api.getFutureResponse("");
      LoginResponse loginResponse =
      LoginResponse.fromJson(jsonDecode(res.data));
      if (loginResponse!= null && loginResponse.status) {
        return loginResponse.content?.loggedUser;
      }
    } catch (e) {
      return null;
    }
  }
}
