import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/login_bloc.dart';
import 'login_form.dart';

class LoginPage extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              colorFilter:
                  ColorFilter.mode(Colors.black.withOpacity(0.1), BlendMode.dstATop),
              image: CachedNetworkImageProvider(
                  'https://images.unsplash.com/photo-1470790376778-a9fbc86d70e2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'),
              fit: BoxFit.cover),
        ),
        child: BlocProvider<LoginBloc>(
          create: (context) =>
              LoginBloc(),
          child: LoginForm(),
        ),
      ),
    );
  }
}
