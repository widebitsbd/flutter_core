import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_core/auth/bloc/bloc.dart';
import 'package:flutter_core/auth/reg/ui/register_page.dart';
import 'package:flutter_core/widget/core_dialog.dart';
import 'package:flutter_core/widget/login_button.dart';
import 'package:flutter_core/utils/lang_util.dart';
import 'package:flutter_core/widget/vertical_text.dart';
import 'package:flutter_core/auth/forgotpass/ui/forgot_password_page.dart';
import 'package:flutter_core/core/app_provider.dart';


import 'bloc/bloc.dart';


class LoginForm extends StatefulWidget {

  LoginForm({Key key}) :super(key: key);

  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController(text: kReleaseMode ? '' : 'test@fluttercore.com');
  final TextEditingController _passwordController = TextEditingController(text: kReleaseMode ? '' : 'abcd1234');

  final FocusNode _focusEmail = FocusNode();
  final FocusNode _focusPassword = FocusNode();

  LoginBloc _loginBloc;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
    if (!kReleaseMode) {
      WidgetsBinding.instance.addPostFrameCallback((_) => _onFormSubmitted());
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state.failureMsg.isNotEmpty)
          CoreDialog().snackBar(context, 'error.login_failed'.lang, isError: true);
        else if (state.isSubmitting)
          CoreDialog().snackBarProgress(context, 'message.logging_in'.lang);
        else if (state.successMsg.isNotEmpty) {
          BlocProvider.of<AuthenticationBloc>(context)..add(LoggedIn());
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return _buildBody(context, state);
        },
      ),
    );
  }

  Widget _buildBody(context, LoginState state) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        VerticalText(text: 'label.login'.lang),
        Expanded(
          child: Form(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 48),
              child: ListView(
                children: <Widget>[
                  Image.asset('assets/images/app_icon_256.png', height: 64),
                  SizedBox(
                    height: 16,
                  ),
                  TextFormField(
                    key: Key('login_email'),
                    controller: _emailController,
                    focusNode: _focusEmail,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        filled: true,
                        icon: Icon(Icons.email),
                        hintText: 'hint.what_is_your_email_id'.lang,
                        labelText: 'label.email'.lang,
                        errorText: state.emailError.lang),
                    autocorrect: false,
                    validator: (_) {
                      return state.emailError ?? null;
                    },
                  ),
                  TextFormField(
                    key: Key('login_password'),
                    controller: _passwordController,
                    focusNode: _focusPassword,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        filled: true,
                        icon: Icon(Icons.lock),
                        hintText: 'hint.enter_your_password'.lang,
                        labelText: 'label.password'.lang,
                        errorText: state.passwordError.lang,
                        suffixIcon: IconButton(
                          icon: Icon(Icons.remove_red_eye),
                          color: state.isShowPassword ? Colors.blue : Colors.grey,
                          onPressed: () {
                            _loginBloc.add(ShowPasswordToggle());
                          },
                        )),
                    obscureText: !state.isShowPassword,
                    autocorrect: false,
                    validator: (_) {
                      return state.passwordError ?? null;
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24),
                    child: Row(
                      children: <Widget>[
                        Checkbox(
                          value: state.isKeepMeLoggedIn,
                          onChanged: (val) {
                            _loginBloc.add(KeepMeLoggedIn(val: val));
                          },
                        ),
                        Text('label.keep_me_logged_in'.lang),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      LoginButton(
                        key: Key('login_button'),
                        text: 'label.login'.lang,
                        onPressed: isLoginButtonEnabled(state) ? _onFormSubmitted : null,
                      ),
                      FlatButton(
                        onPressed: () {
                          AppProvider.getRouter(context)
                              .navigateTo(context, ForgotPasswordPage.PATH);
                        },
                        child: Text('label.forgot_password'.lang),
                      ),
                      FlatButton(
                        onPressed: () {
                          AppProvider.getRouter(context)
                              .navigateTo(context, RegisterPage.PATH);
                        },
                        child: Text('label.create_an_account'.lang),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _loginBloc.add(
      EmailChanged(email: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _loginBloc.add(
      PasswordChanged(password: _passwordController.text),
    );
  }

  void _onFormSubmitted() {
    _loginBloc.add(
      LoginWithCredentialsPressed(
        email: _emailController.text,
        password: _passwordController.text,
      ),
    );
  }
}
