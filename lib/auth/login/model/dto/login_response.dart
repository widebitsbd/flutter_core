import 'package:flutter_core/user/dto/logged_user.dart';

class LoginResponse {
  bool status;
  String message;
  Content content;

  LoginResponse({this.status, this.message, this.content});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    content = json['content'] != null ? new Content.fromJson(json['content']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.content != null) {
      data['content'] = this.content.toJson();
    }
    return data;
  }
}

class Content {
  LoggedUser loggedUser;
  String token;

  Content({this.loggedUser, this.token});

  Content.fromJson(Map<String, dynamic> json) {
    loggedUser = json['userdata'] != null ? new LoggedUser.fromJson(json['userdata']) : null;
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.loggedUser != null) {
      data['userdata'] = this.loggedUser.toJson();
    }
    data['token'] = this.token;
    return data;
  }
}



class LogoutResponse {
  bool status;
  String message;
  String content;

  LogoutResponse({this.status, this.message, this.content});

  LogoutResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    content = json['content'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['content'] = this.content;
    return data;
  }
}
