import 'dart:async';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core/api/base_url.dart';
import 'package:flutter_core/core/app.dart';
import 'package:flutter_core/core/app_component.dart';

enum EnvType { DEVELOPMENT, STAGING, PRODUCTION }

class Env {
  static Env value;
  String appName;
  String baseUrl;
  Color primarySwatch;
  EnvType environmentType = EnvType.DEVELOPMENT;
  App application;

  Env(App app) {
    this.application = app;
    value = this;
    _init();
  }

  _init() async {
    WidgetsFlutterBinding.ensureInitialized();
    await application.onCreate();
    BaseUrl.baseURL = baseUrl;

    runZonedGuarded(() {
      runApp(EasyLocalization(supportedLocales: [
        Locale('en', 'US'),
        Locale('bn', 'BD'),
      ], path: 'langs', child: AppComponent(application)));
    }, (Object error, StackTrace stack) {
      print('Caught error: $error');
    });
  }
}
