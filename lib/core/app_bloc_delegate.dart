
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_core/logger/Log.dart';


class AppBlocDelegate extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    Log.d('$event fired!');
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    Log.d(transition);
  }

  @override
  void onError(Cubit bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    Log.e(error.toString() + bloc.toString(), error, stacktrace);
  }
}
