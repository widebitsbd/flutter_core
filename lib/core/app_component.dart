import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_core/core/app.dart';
import 'package:flutter_core/logger/Log.dart';
import 'package:flutter_core/theme/bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'app_provider.dart';
import 'env.dart';

class AppComponent extends StatefulWidget {
  final App _application;

  AppComponent(this._application);

  @override
  State createState() => AppComponentState(_application);
}

class AppComponentState extends State<AppComponent> {
  final App _application;

  AppComponentState(this._application);

  @override
  void dispose() async {
    Log.i('dispose');
    super.dispose();
    await _application.onTerminate();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ThemeBloc>(
      create: (context) => ThemeBloc(server: _application.getAppPreference()),
      child: _appProvider(),
    );
  }

  Widget _appProvider() {
    return AppProvider(
        child: BlocBuilder<ThemeBloc, ThemeState>(
          builder: (context, themeState) {
            return _materialApp(themeState);
          },
        ),
        application: _application);
  }

  Widget _materialApp(themeState) {
    return MaterialApp(
      title: Env.value.appName,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        EasyLocalization.of(context).delegate,
      ],
      supportedLocales: EasyLocalization.of(context).supportedLocales,
      locale: EasyLocalization.of(context).locale,
      debugShowCheckedModeBanner: false,
      theme: themeState.theme,
      onGenerateRoute: _application.router.generator,
    );
  }
}
