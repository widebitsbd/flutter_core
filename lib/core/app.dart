import 'package:fluro/fluro.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_core/api/api_client.dart';
import 'package:flutter_core/auth/service/auth_service.dart';
import 'package:flutter_core/logger/Log.dart';
import 'package:flutter_core/pref/app_pref.dart';
import 'package:flutter_core/pref/db_keys.dart';
import 'package:flutter_core/user/dto/logged_user.dart';
import 'package:flutter_core/user/repo/firebase_user_repository.dart';
import 'package:logger/logger.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'application.dart';

class App implements Application {
  FluroRouter router;
  Database _db;
  ApiClient _apiClient;
  AuthService authService;
  AppPreference _pref;
  LoggedUser loggedInUser;
  FirebaseUserRepository userRepository;
  

  @override
  Future<void> onCreate() async {
    _initLog();
    _initRouter();
    await _initDB();
    _initAPIClient();
    await _initPreference();
    _initUserRepository();
    _initAuthService();
  }

  @override
  Future<void> onTerminate() async {
    await _db.close();
  }

  void _initLog() {
    Log.init();
    Log.setLevel(Level.verbose);
  }

  void _initRouter() {
    router = FluroRouter();
  }

  Future<void> _initUserRepository() async {
   // await Firebase.initializeApp();
    userRepository = FirebaseUserRepository();
  }

  void _initAuthService() {
    authService = AuthService(_apiClient, _db, _pref);
  }


  FluroRouter getRouter() {
    if (router == null) router = FluroRouter();
    return router;
  }

  Future<void> _initDB() async {
    if (!kIsWeb) {
      final dir = await getApplicationDocumentsDirectory();
      await dir.create(recursive: true);
      var dbPath = join(dir.path, DbKeys.DB_NAME);
      _db = await databaseFactoryIo.openDatabase(dbPath);
    }
  }

  void _initAPIClient() {
    _apiClient = ApiClient();
  }

  ApiClient getApiClient() {
    if (_apiClient == null) _initAPIClient();
    return _apiClient;
  }

  Future<void> _initPreference() async {
    _pref = AppPreference();
    await _pref.init();
  }

  AppPreference getAppPreference() {
    if (_pref == null) _initPreference();
    return _pref;
  }

  void setLoggedInUser(LoggedUser user) {
    loggedInUser = user;
  }

}
