class LoggedUser {
  int id;
  String name;
  String username;

  LoggedUser({this.id, this.name, this.username});

  LoggedUser.fromJson(Map<String, dynamic> json) {
    id = json['ID'];
    name = json['name'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ID'] = this.id;
    data['name'] = this.name;
    data['username'] = this.username;
    return data;
  }

  @override
  String toString() {
    return 'LoggedUser{id: $id, name: $name, username: $username}';
  }
}
