import 'dart:ffi';

import 'package:flutter_core/user/dto/logged_user.dart';
import 'package:flutter_core/user/repo/user_repository.dart';

class FirebaseUserRepository extends UserRepository {
  // final FirebaseAuth _firebaseAuth;
  // final GoogleSignIn _googleSignIn;
  // final CollectionReference _usercollection;

  // FirebaseUserRepository({FirebaseAuth firebaseAuth, GoogleSignIn googleSignin})
  //     : _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance,
  //       _googleSignIn = googleSignin ?? GoogleSignIn(),
  //       _usercollection = FirebaseFirestore.instance.collection('users');

  Future<LoggedUser> signInWithGoogle() async {
    return LoggedUser();
  }

  Future<Void> signUp(
      {String email, String firstName, String lastName, String password}) async {
    // UserCredential result = await _firebaseAuth.createUserWithEmailAndPassword(
    //   email: email,
    //   password: password,
    // );
    // _usercollection.doc(result.user.uid).set(LoggedUser(
    //     id: id,
    //     name: firstName,
    //     username: result.user.email)
    //     .toEntity()
    //     .toDocument());
    return null;
  }

  @override
  Future<void> addNewUser(LoggedUser item, String password) {
    throw UnimplementedError();
  }

  @override
  Future<void> authenticate() {
    throw UnimplementedError();
  }

  @override
  Future<void> deleteUser(LoggedUser item) {
    throw UnimplementedError();
  }

  @override
  Future<LoggedUser> getUserById(String id) {
    throw UnimplementedError();
  }

  @override
  Future<String> getUserId() {
    throw UnimplementedError();
  }

  @override
  Future<bool> isSignedIn() {
    throw UnimplementedError();
  }

  @override
  Future<void> updateUser(LoggedUser item) {
    throw UnimplementedError();
  }

  @override
  Stream<List<LoggedUser>> users() {
    throw UnimplementedError();
  }

}
