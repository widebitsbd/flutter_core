import 'dart:async';

import 'package:flutter_core/user/dto/logged_user.dart';


abstract class UserRepository {
  Future<bool> isSignedIn();

  Future<void> authenticate();

  Future<String> getUserId();

  Stream<List<LoggedUser>> users();

  Future<void> updateUser(LoggedUser item);

  Future<LoggedUser> getUserById(String id);

  Future<void> addNewUser(LoggedUser item, String password);

  Future<void> deleteUser(LoggedUser item);
}
