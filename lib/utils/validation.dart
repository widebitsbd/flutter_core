import 'package:flutter_core/utils/lang_util.dart';
class Validation {
  static bool _isNameValid(String value) {
    final RegExp nameExp = RegExp(r'^[A-za-z ]+$');
    return nameExp.hasMatch(value);
  }

  static String getNameValid(String name) {
    if (name.isNullOrEmpty) return 'error.name_empty';
    if (!_isNameValid(name)) return 'error.name_invalid_character';
    return '';
  }

  static bool _isEmailValid(String email) {
    String emailValidationRule =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(emailValidationRule);
    return regExp.hasMatch(email);
  }

  static String getEmailValid(String email) {
    if (email.isNullOrEmpty) return 'error.email_required';
    if (!_isEmailValid(email)) return 'error.email_format';
    return '';
  }

  static bool isPhoneValid(String phone) {
    String phoneValidationRule = r'^\+?[0-9]{3}-?[0-9]{6,12}$';
    RegExp regExp = new RegExp(phoneValidationRule);
    return regExp.hasMatch(phone);
  }

  static String getPasswordValid(String value) {
    if (value.isEmpty) return 'error.password_empty';
    if (value.length < 8) return 'error.password_min_length';
    return '';
  }

  String isValidPhone(String countryCode, String value) {
    return isPhoneValid(value) ? null : 'Please enter valid phone number';
  }

  String isIdValid(String value) {
    if (value.isEmpty) return 'Please enter id';
    return null;
  }

  String isFilled(String value) {
    return value == null || value.isEmpty ? 'This field is required' : null;
  }

  String isPinValid(String value) {
    if (value.isEmpty) return 'Please enter pin';
    if (value.length < 4) return 'Please enter at least 4 digits pin';
    return null;
  }

  String nameValid(String value) {
    if (value.isEmpty) return 'Please enter name';
    final RegExp nameExp = RegExp(r'^[A-za-z .-]+$');
    return nameExp.hasMatch(value) ? null : 'Please enter valid name';
  }
}
