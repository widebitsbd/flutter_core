import 'package:easy_localization/easy_localization.dart';

extension TextUtilsStringExtension on String {

  bool get isNullOrEmpty => this == null || this.isEmpty || this.trim().isEmpty;

  String get lang => this.isNullOrEmpty ? null : tr(this);
}
