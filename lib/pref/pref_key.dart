class PrefKey{
  static const String IS_ON_BOARDING = 'is_on_boarding';
  static const String IS_LOGGED_IN = 'is_logged_in';
  static const String LOGGED_USER_ID = 'logged_user_id';
  static const String IS_DARK_THEME = 'is_dark_theme';

}